package com.instagaon.instagaon;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import static com.instagaon.instagaon.Utils.getLogMsgId;


public class LoginActivity extends AppCompatActivity {
    /* URL saved to be loaded after fb login */
    private static final String target_url="https://www.instagram.com/";
    private static final String target_url_prefix="www.instagram.com";
    private Context mContext;
    private WebView mWebview;
    private WebView mWebviewPop;
    private FrameLayout mContainer;
    private RelativeLayout mLoadingProgressScreen;
    private LinearLayout mLoginClickScreen;
    public boolean isLoginButtonClickedOnce;
    private TextView followVilleLogoLoadingScreen;
    private String firstDisplay;

    class LoadListener{

        public String getViewer (Document doc) {
            String viewer = null;

            //Log.d("TEMPTAG"+getLogMsgId(), doc.select("script").toString());
            Elements scripts = doc.select("script");
            for (Element script : scripts) {
                for (DataNode node : script.dataNodes()) {
                    String val[] = node.getWholeData().split(" = ");
                    if (!val[0].contentEquals("window._sharedData"))
                        continue;
                    try {
                        JSONObject obj = new JSONObject(val[1]);
                        //Log.d("TEMPTAG"+getLogMsgId(), obj.getString("config"));
                        JSONObject config = new JSONObject(obj.getString("config"));
                        if (!config.isNull("viewer")) {
                            viewer = config.getString("viewer");
                            Log.d("TEMPTAG"+getLogMsgId(), viewer);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    /*
                    for (String s : val) {
                        Log.d("TEMPTAG"+getLogMsgId(), s);
                    }
                    */
                }
            }
            return viewer;
        }

        public String getViewerId(String viewer) {
            JSONObject viewerJO = null;
            String viewerId = null;
            try {
                viewerJO = new JSONObject(viewer);
                viewerId = viewerJO.getString("username");
                if (viewerId == null) {
                    Log.e("CHECK SOURCE"+getLogMsgId(), "username is null for logged in user");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return viewerId;
        }

        public String getViewerFullName(String viewer) {
            JSONObject viewerJO = null;
            String viewerFullName = null;
            try {
                viewerJO = new JSONObject(viewer);
                viewerFullName = viewerJO.getString("full_name");
                if (viewerFullName == null) {
                    Log.e("CHECK SOURCE"+getLogMsgId(), "fullName is null for logged in user");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return viewerFullName;
        }

        @JavascriptInterface
        public void processHTML(String response)
        {
            Document doc = Jsoup.parse(response);
            String title = doc.title();
            Log.d("TEMPTAG"+getLogMsgId(), title);

            String viewer = getViewer(doc);
            if (viewer == null) {
                Log.d("TEMPTAG"+getLogMsgId(), "Not logged In");
                LoginActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isLoginButtonClickedOnce == false) {
                            mLoginClickScreen.setVisibility(View.VISIBLE);
                        }
                        mLoadingProgressScreen.setVisibility(View.INVISIBLE);
                    }
                });
            } else {
                Log.d("TEMPTAG"+getLogMsgId(), viewer);

                String viewerId = getViewerId(viewer);
                String viewerFullName = getViewerFullName(viewer);
                String userToBeFollowed = "avra.sengupta1";

                if (viewerId != null) {
                    Log.d("TEMPTAG"+getLogMsgId(), viewerId);
                }

                if (viewerFullName != null) {
                    Log.d("TEMPTAG"+getLogMsgId(), viewerFullName);
                }

                /*
                Get user name for following

                InitializeGTTB iGTTB = new InitializeGTTB();
                iGTTB.initGTTB();
                */

                if ((viewerId != null) && (viewerFullName != null) && (userToBeFollowed != null)) {
                    //goToFollowActivity(viewerId, viewerFullName, userToBeFollowed);
                    goToGenTicketActivity(viewerId, viewerFullName);
                } else {
                    Log.e("CHECK CODE & SOURCE"+getLogMsgId(), "One of the parameters is null : userId, fullName, userToBeFollowed");
                }
            }
        }
    }

    public void goToFollowActivity(String viewerId, String viewerFullName, String userToBeFollowed) {
        Intent followIntent = new Intent(this, FollowActivity.class);
        followIntent.putExtra("VIEWER_ID", viewerId);
        followIntent.putExtra("VIEWER_FULL_NAME", viewerFullName);
        followIntent.putExtra("USER_TO_BE_FOLLOWED", userToBeFollowed);
        followIntent.putExtra("FIRST_PAGE_OPEN", "yes");
        startActivity(followIntent);
        finish();
    }

    public void goToGenTicketActivity(String viewerId, String viewerFullName) {
        Intent genTcktIntent = new Intent(this, GenTicket.class);
        genTcktIntent.putExtra("VIEWER_ID", viewerId);
        genTcktIntent.putExtra("VIEWER_FULL_NAME", viewerFullName);
        startActivity(genTcktIntent);
        finish();
    }

    public void reloadLoginActivityWithMsg (String msg) {
        Intent loginActivityIntent = new Intent(this, LoginActivity.class);
        loginActivityIntent.putExtra("FIRST_DISPLAY", msg);
        startActivity(loginActivityIntent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        isLoginButtonClickedOnce = false;

        Button loginInstaButton = (Button)findViewById(R.id.log_in_button);

        mLoadingProgressScreen = (RelativeLayout)findViewById(R.id.loadingScreenRelative);
        mLoadingProgressScreen.setVisibility(View.VISIBLE);

        mLoginClickScreen = (LinearLayout) findViewById(R.id.directUserToLoginLinear);
        mLoginClickScreen.setVisibility(View.INVISIBLE);

        loginInstaButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("TEMPTAG"+getLogMsgId(), "Clicked on the Log In Button");
                mLoginClickScreen.setVisibility(View.INVISIBLE);
                isLoginButtonClickedOnce = true;
                followVilleLogoLoadingScreen.setVisibility(View.VISIBLE);
            }
        });

        followVilleLogoLoadingScreen = (TextView)findViewById(R.id.followVilleLogo);
        firstDisplay = getIntent().getStringExtra("FIRST_DISPLAY");
        if (firstDisplay != null) {
            if (firstDisplay.equals("SET_EMPTY")) {
                followVilleLogoLoadingScreen.setVisibility(View.INVISIBLE);
            } else {
                followVilleLogoLoadingScreen.setText(firstDisplay);
            }
        }

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);

        mWebview = (WebView) findViewById(R.id.instaLoginWebview);
        //mWebviewPop = (WebView) findViewById(R.id.webviewPop);
        mContainer = (FrameLayout) findViewById(R.id.instaLoginWebviewFrame);
        WebSettings webSettings = mWebview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);

        mWebview.addJavascriptInterface(new LoadListener(), "HTMLOUT");
        cookieManager.setAcceptThirdPartyCookies(mWebview,true);

        mWebview.setWebViewClient(new UriWebViewClient());
        mWebview.setWebChromeClient(new UriChromeClient());

        Log.d ("TEMPTAG"+getLogMsgId(), "About to load url");
        mWebview.loadUrl(target_url);
        Log.d ("TEMPTAG"+getLogMsgId(), "Finished loading url");

        mContext=this.getApplicationContext();
    }

    private class UriWebViewClient extends WebViewClient {

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            Log.d ("TEMPTAG"+getLogMsgId(), "Started Loading Page");
            LoginActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isLoginButtonClickedOnce == true) {
                        followVilleLogoLoadingScreen.setText("Processing");
                    }
                    mLoadingProgressScreen.setVisibility(View.VISIBLE);
                }
            });
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            String host = Uri.parse(url).getHost();
            Log.d ("TEMPTAG"+getLogMsgId(), "url = "+url);
            Log.d ("TEMPTAG"+getLogMsgId(), "Inside shouldOverrideUrlLoading");
            if (url.equals(target_url) || url.equals(target_url+"#reactivated") || url.contains("accounts/signup"))
            {
                // This is my web site, so do not override; let my WebView load
                // the page
                Log.d ("TEMPTAG"+getLogMsgId(), "host equals target_prefix");
                if(mWebviewPop!=null)
                {
                    mWebviewPop.setVisibility(View.GONE);
                    mContainer.removeView(mWebviewPop);
                    mWebviewPop=null;
                    Log.d ("TEMPTAG"+getLogMsgId(), "Removing mWebview pop from container");
                }
                Log.d ("TEMPTAG"+getLogMsgId(), "Returning false");
                return false;
            }

            if(host.equals("m.facebook.com")|| host.equals("www.facebook.com") || host.equals("facebook.com"))
            {
                Log.d ("TEMPTAG"+getLogMsgId(), "Returning false as host equals facebook");
                LoginActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (isLoginButtonClickedOnce == true) {
                            followVilleLogoLoadingScreen.setText("Processing");
                        }
                        mLoadingProgressScreen.setVisibility(View.VISIBLE);
                    }
                });
                return false;
            }
            // Otherwise, the link is not for a page on my site, so launch
            // target_url again
            reloadLoginActivityWithMsg("SET_EMPTY");
            return true;
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                       SslError error) {
            Log.d("onReceivedSslError"+getLogMsgId(), "onReceivedSslError");
            //super.onReceivedSslError(view, handler, error);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.d ("TEMPTAG"+getLogMsgId(), "At on Page Finished");
            LoginActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (isLoginButtonClickedOnce == true) {
                        followVilleLogoLoadingScreen.setText("Processing");
                    }
                    mLoadingProgressScreen.setVisibility(View.VISIBLE);
                }
            });
            view.loadUrl("javascript:HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
        }
    }

    class UriChromeClient extends WebChromeClient {

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog,
                                      boolean isUserGesture, Message resultMsg) {
            mWebviewPop = new WebView(mContext);
            mWebviewPop.getSettings().setJavaScriptEnabled(true);
            mWebviewPop.getSettings().setSavePassword(false);
            mWebviewPop.setVerticalScrollBarEnabled(false);
            mWebviewPop.setHorizontalScrollBarEnabled(false);
            mWebviewPop.setWebViewClient(new UriWebViewClient());
            mWebviewPop.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            mContainer.addView(mWebviewPop);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(mWebviewPop);
            resultMsg.sendToTarget();

            Log.d ("TEMPTAG"+getLogMsgId(), "send to target complete. Returning true");

            return true;
        }

        @Override
        public void onCloseWindow(WebView window) {
            Log.d("onCloseWindow"+getLogMsgId(), "called");
        }

    }
}