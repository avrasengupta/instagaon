package com.instagaon.instagaon;

/**
 * Created by Avra Sengupta on 10/26/2017.
 */

public class Utils {
    /*
     * Get the current file name, method name and line number.
     */
    public static String getLogMsgId() {
        String msg_id = " "+Thread.currentThread().getStackTrace()[3].getFileName() +
                " : " + Thread.currentThread().getStackTrace()[3].getMethodName() +
                " : " + Thread.currentThread().getStackTrace()[3].getLineNumber() +
                " : ";
        return msg_id;
    }
}
