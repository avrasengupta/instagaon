package com.instagaon.instagaon;

import android.util.Log;

import com.amazonaws.http.HttpMethodName;
import com.amazonaws.mobileconnectors.apigateway.ApiClientFactory;
import com.amazonaws.mobileconnectors.apigateway.ApiRequest;
import com.amazonaws.mobileconnectors.apigateway.ApiResponse;
import com.instagaon.androidsdk.GetTicketToBeProcessedClient;
import com.instagaon.androidsdk.model.Empty;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.xml.transform.Result;

import static com.instagaon.instagaon.Utils.getLogMsgId;

/**
 * Created by Avra Sengupta on 11/5/2017.
 */

public class InitializeGTTB {
    public void initGTTB () {
        ApiClientFactory factory = new ApiClientFactory().apiKey("jXPEIdT0rrdHvRwEt4Erad1bnY42gkl3ODD3qLua");

        final GetTicketToBeProcessedClient client = factory.build(GetTicketToBeProcessedClient.class);

        ApiRequest request = new ApiRequest();
        request.withHttpMethod(HttpMethodName.GET);
        request.withPath("/getTicketToBeProcessed");
        request.withParameter("viewer", "efgh");

        ApiResponse response = client.execute(request);
        int responseCode = response.getStatusCode();
        Log.d("TEMPTAG"+getLogMsgId(), "request path: "+request.getPath());
        Log.d("TEMPTAG"+getLogMsgId(), "request httpmethod: "+request.getHttpMethod());
        Log.d("TEMPTAG"+getLogMsgId(), "responseCode = "+responseCode);
        Log.d("TEMPTAG"+getLogMsgId(), "responseStatusText = "+response.getStatusText());
        String responseBody = "empty";
        try {
            responseBody = convertStreamToString(response.getRawContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
        Log.d("TEMPTAG"+getLogMsgId(),responseBody);
    }

    private String convertStreamToString (InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null){
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return sb.toString();
    }
}
