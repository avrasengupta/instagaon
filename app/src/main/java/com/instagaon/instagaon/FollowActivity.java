package com.instagaon.instagaon;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.CookieManager;
import android.webkit.JavascriptInterface;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.DataNode;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import static com.instagaon.instagaon.Utils.getLogMsgId;

public class FollowActivity extends AppCompatActivity {
    /* URL saved to be loaded after fb login */
    private String target_url="https://www.instagram.com";
    private String target_url_prefix="www.instagram.com";
    private WebView mWebview;
    private String viewerId;
    private String viewerFullName;
    private String userToBeFollowed;
    private String firstPageOpen;
    private Context mContext;
    private WebView mWebviewPop;
    private FrameLayout mContainer;
    private int isFirstResponse = 1;

    class LoadListener{

        public String getWindowSharedData (Document doc) {
            String windowSharedData = null;

            Elements scripts = doc.select("script");
            for (Element script : scripts) {
                for (DataNode node : script.dataNodes()) {
                    String val[] = node.getWholeData().split(" = ");
                    if (!val[0].contentEquals("window._sharedData"))
                        continue;
                    windowSharedData = val[1];
                }
            }

            return windowSharedData;
        }

        public String getViewer (String windowSharedData) {
            String viewer = null;

            try {
                JSONObject obj = new JSONObject(windowSharedData);
                JSONObject config = new JSONObject(obj.getString("config"));
                if (!config.isNull("viewer")) {
                    viewer = config.getString("viewer");
                    Log.d("TEMPTAG"+getLogMsgId(), viewer);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return viewer;
        }


        public String getUserDetails (String windowSharedData) {
            String userDetails = null;

            try {
                JSONObject wSD = new JSONObject(windowSharedData);
                if (!wSD.isNull("entry_data")) {
                    JSONObject entryData = new JSONObject(wSD.getString("entry_data"));
                    if (!entryData.isNull("ProfilePage")) {
                        JSONArray profilePage = new JSONArray(entryData.getString("ProfilePage"));
                        JSONObject user = profilePage.getJSONObject(0);
                        userDetails = user.getString("user");
                        Log.d ("TEMPTAG"+getLogMsgId(), "userDetail = "+userDetails);
                    } else {
                        Log.e("CHECK SOURCE"+getLogMsgId(), "ProfilePage is missing in entry_data in "+target_url);
                    }
                } else {
                    Log.e("CHECK SOURCE"+getLogMsgId(), "entry_data is missing in "+target_url);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return userDetails;
        }
        public String getLoadedUser (String userDetails) {
            String loadedUser = null;

            try {
                JSONObject uD = new JSONObject(userDetails);
                Log.d("TEMPTAG"+getLogMsgId(), "uD = "+uD.toString());
                if (!uD.isNull("username")) {
                    loadedUser = uD.getString("username");
                    Log.d("TEMPTAG"+getLogMsgId(), loadedUser);
                } else {
                    Log.e("CHECK SOURCE"+getLogMsgId(), "loadedUser is missing in "+target_url);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return loadedUser;
        }

        public String getLoadedUserFollowedByViewerStatus (String userDetails) {
            String loadedUserFollowedByViewer = "false";

            try {
                JSONObject uD = new JSONObject(userDetails);
                if (!uD.isNull("followed_by_viewer")) {
                    loadedUserFollowedByViewer = uD.getString("followed_by_viewer");
                    Log.d("TEMPTAG"+getLogMsgId(), loadedUserFollowedByViewer);
                } else {
                    Log.e("CHECK SOURCE"+getLogMsgId(), "followed_by_viewer is missing in "+target_url);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return loadedUserFollowedByViewer;
        }

        public String getViewerId(String viewer) {
            JSONObject viewerJO = null;
            String viewerId = null;
            try {
                viewerJO = new JSONObject(viewer);
                viewerId = viewerJO.getString("username");
                if (viewerId == null) {
                    Log.e("CHECK SOURCE"+getLogMsgId(), "username is null for logged in user");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return viewerId;
        }

        public String getViewerFullName(String viewer) {
            JSONObject viewerJO = null;
            String viewerFullName = null;
            try {
                viewerJO = new JSONObject(viewer);
                viewerFullName = viewerJO.getString("full_name");
                if (viewerFullName == null) {
                    Log.e("CHECK SOURCE"+getLogMsgId(), "fullName is null for logged in user");
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return viewerFullName;
        }

        @JavascriptInterface
        public void processHTML(String response)
        {
            Document doc = Jsoup.parse(response);
            String title = doc.title();
            Log.d("TEMPTAG"+getLogMsgId(), title);

            Log.d("TEMPTAG"+getLogMsgId(), doc.select("script").toString());

            String windowSharedData = getWindowSharedData(doc);
            if (windowSharedData == null){
                Log.e("CHECK SOURCE"+getLogMsgId(), "windowSharedData is null");
                return;
            }

            Log.d("TEMPTAG"+getLogMsgId(), "window._sharedData = "+windowSharedData);

            String viewer = getViewer(windowSharedData);
            if (viewer == null) {
                Log.d("TEMPTAG"+getLogMsgId(), "Not logged In");
            } else {
                Log.d("TEMPTAG"+getLogMsgId(), viewer);

                String loadedViewerId = getViewerId(viewer);
                String loadedViewerFullName = getViewerFullName(viewer);
                String userDetails = getUserDetails(windowSharedData);
                String loadedUserName = getLoadedUser(userDetails);
                String loadedUserFollowedByViewer = getLoadedUserFollowedByViewerStatus(userDetails);

                if (loadedViewerId != null) {
                    Log.d("TEMPTAG"+getLogMsgId(), loadedViewerId);
                }

                if (loadedViewerFullName != null) {
                    Log.d("TEMPTAG"+getLogMsgId(), loadedViewerFullName);
                }

                if (loadedUserName != null) {
                    Log.d("TEMPTAG"+getLogMsgId(), loadedUserName);
                }

                if ((loadedViewerId != null) && (loadedViewerFullName != null) && (loadedUserName != null)) {
                    if (!loadedViewerFullName.equals(viewerFullName)) {
                        Log.e("CHECK SOURCE"+getLogMsgId(), "loadedViewerFullName != viewerFullName");
                    }

                    if (!loadedUserName.equals(userToBeFollowed)) {
                        Log.e("CHECK SOURCE"+getLogMsgId(), "loadedUserName != userToBeFollowed");
                    }

                    if (!loadedViewerId.equals(viewerId)) {
                        Log.e("CHECK SOURCE"+getLogMsgId(), "loadedViewerId != viewerId");
                    }

                    if (loadedUserFollowedByViewer.equals("true")) {
                        if (firstPageOpen.equals("yes")) {
                            Log.e("ALREADY FOLLOWING USER"+getLogMsgId(),
                                    viewerId+" WAS ALREADY FOLLOWING "+userToBeFollowed+" GET ANOTHER USER TO BE FOLLOWED ");
                            // CODE TO GET ANOTHER USER TO BE FOLLOWED

                            //CODE TO CALL FOLLOW ACTIVITY AGAIN
                        }else {
                            Log.d("TEMPTAG"+getLogMsgId(), viewerId+" FOLLOWS "+userToBeFollowed);
                        }
                    }else {
                        Log.d("TEMPTAG"+getLogMsgId(), viewerId+" DOES NOT FOLLOW "+userToBeFollowed);
                    }


                } else {
                    Log.e("CHECK CODE & SOURCE"+getLogMsgId(), "One of the parameters is null : loadedViewerId, loadedViewerFullName, loadedUserName");
                }
            }
            return;
        }
    }

    public void goToFollowActivity() {
        Intent followIntent = new Intent(this, FollowActivity.class);
        followIntent.putExtra("VIEWER_ID", viewerId);
        followIntent.putExtra("VIEWER_FULL_NAME", viewerFullName);
        followIntent.putExtra("USER_TO_BE_FOLLOWED", userToBeFollowed);
        followIntent.putExtra("FIRST_PAGE_OPEN", "no");
        startActivity(followIntent);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow);

        CookieManager cookieManager = CookieManager.getInstance();
        cookieManager.setAcceptCookie(true);

        viewerId = getIntent().getStringExtra("VIEWER_ID");
        viewerFullName = getIntent().getStringExtra("VIEWER_FULL_NAME");
        userToBeFollowed = getIntent().getStringExtra("USER_TO_BE_FOLLOWED");
        firstPageOpen = getIntent().getStringExtra("FIRST_PAGE_OPEN");

        if (firstPageOpen != null) {
            Log.d("TEMPTAG"+getLogMsgId(), "firstPageOpen = "+firstPageOpen);
        } else {
            Log.e("VERIFY PUT_EXTRA"+getLogMsgId(), "firstPageOpen is null");
        }

        if (viewerId != null) {
            Log.d("TEMPTAG"+getLogMsgId(), viewerId);
        } else {
            Log.e("VERIFY PUT_EXTRA"+getLogMsgId(), "viewerId is null");
        }

        if (viewerFullName != null) {
            Log.d("TEMPTAG"+getLogMsgId(), viewerFullName);
        } else {
            Log.e("VERIFY PUT_EXTRA"+getLogMsgId(), "viewerFullName is null");
        }

        if (userToBeFollowed != null) {
            Log.d("TEMPTAG"+getLogMsgId(), userToBeFollowed);
        } else {
            Log.e("VERIFY PUT_EXTRA"+getLogMsgId(), "userToBeFollowed is null");
        }

        target_url += "/"+userToBeFollowed+"/";

        mWebview = (WebView) findViewById(R.id.instaFollowWebview);
        mContainer = (FrameLayout) findViewById(R.id.instaFollowWebviewFrame);
        WebSettings webSettings = mWebview.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);

        mContext=this.getApplicationContext();
        mWebview.addJavascriptInterface(new LoadListener(), "HTMLOUT");
        cookieManager.setAcceptThirdPartyCookies(mWebview,true);

        mWebview.setWebViewClient(new UriWebViewClient());
        mWebview.setWebChromeClient(new UriChromeClient());

        Log.d ("TEMPTAG"+getLogMsgId(), "About to load url");
        mWebview.loadUrl(target_url);
        Log.d ("TEMPTAG"+getLogMsgId(), "Finished loading url");

    }

    private class UriWebViewClient extends WebViewClient {
        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
            if (request.getUrl().toString().contains("SubscribedButtonClick")) {
                Log.d("TEMPTAG"+getLogMsgId(), "Will reload activity");
                goToFollowActivity();
            }
            //Log.d("TEMPTAG"+getLogMsgId(), request.toString());
            //Log.d("TEMPTAG"+getLogMsgId(), "shouldInterceptRequest Called : request.getUrl()="+request.getUrl());
            return super.shouldInterceptRequest(view, request);
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler,
                                       SslError error) {
            Log.d("onReceivedSslError"+getLogMsgId(), "onReceivedSslError");
            //super.onReceivedSslError(view, handler, error);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            Log.d ("TEMPTAG"+getLogMsgId(), "At on Page Finished");
            if (isFirstResponse == 1) {
                Log.d("TEMPTAG"+getLogMsgId(), "Next pagefinish we will move to another activity");
                isFirstResponse = 0;
                view.addJavascriptInterface(new Object(){
                    @JavascriptInterface
                    public void performClick() throws Exception {
                        Log.d("TEMPTAG"+getLogMsgId(), "Click Detected");
                        goToFollowActivity();
                    }
                }, "detectClick");
            } else {
                Log.d("TEMPTAG"+getLogMsgId(), "Reloading Follow Activity");
                goToFollowActivity();
            }
            view.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
        }
    }

    class UriChromeClient extends WebChromeClient {

        @Override
        public boolean onCreateWindow(WebView view, boolean isDialog,
                                      boolean isUserGesture, Message resultMsg) {
            mWebviewPop = new WebView(mContext);
            mWebviewPop.getSettings().setJavaScriptEnabled(true);
            mWebviewPop.getSettings().setSavePassword(false);
            mWebviewPop.setVerticalScrollBarEnabled(false);
            mWebviewPop.setHorizontalScrollBarEnabled(false);
            mWebviewPop.setWebViewClient(new UriWebViewClient());
            mWebviewPop.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
            mContainer.addView(mWebviewPop);
            WebView.WebViewTransport transport = (WebView.WebViewTransport) resultMsg.obj;
            transport.setWebView(mWebviewPop);
            resultMsg.sendToTarget();

            Log.d ("TEMPTAG"+getLogMsgId(), "send to target complete. Returning true");

            return true;
        }

        @Override
        public void onCloseWindow(WebView window) {
            Log.d("onCloseWindow"+getLogMsgId(), "called");
        }

    }

}
