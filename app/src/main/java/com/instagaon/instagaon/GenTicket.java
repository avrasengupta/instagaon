package com.instagaon.instagaon;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.view.ContextThemeWrapper;
import android.support.v7.widget.PopupMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

public class GenTicket extends AppCompatActivity {
    private String viewerId;
    private String viewerFullName;
    private FrameLayout dimLayout;
    private Button genTicketButton;
    private ProgressBar genTcktProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gen_ticket);

        dimLayout = (FrameLayout) findViewById(R.id.dimmerFrame);
        dimLayout.setVisibility(View.VISIBLE);
        dimLayout.getForeground().setAlpha(0);

        genTicketButton = (Button) findViewById(R.id.genTcktButton);
        genTcktProgressBar = (ProgressBar)findViewById(R.id.genTcktBtnPB);

        viewerId = getIntent().getStringExtra("VIEWER_ID");
        viewerFullName = getIntent().getStringExtra("VIEWER_FULL_NAME");
    }

    public void generateTicketClicked (View v) {
        genTicketButton.setText("Fetching User To Be Followed");
        genTicketButton.setClickable(false);
        genTicketButton.setBackgroundColor(getResources().getColor(R.color.lightGrey));
        genTcktProgressBar.setVisibility(View.VISIBLE);
    }

    public void signOut() {
        clearCookies(this);
        Intent loginActivityIntent = new Intent(this, LoginActivity.class);
        loginActivityIntent.putExtra("FIRST_DISPLAY", "Logging Out");
        startActivity(loginActivityIntent);
        finish();
    }

    public void onClickSignOutButton (View v) {
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle("Log Out")
                .setMessage("Do you really want to log out?")
                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        signOut();
                    }
                })
                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    @SuppressWarnings("deprecation")
    public static void clearCookies(Context context)
    {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP_MR1) {
            CookieManager.getInstance().removeAllCookies(null);
            CookieManager.getInstance().flush();
        } else
        {
            CookieSyncManager cookieSyncMngr= CookieSyncManager.createInstance(context);
            cookieSyncMngr.startSync();
            CookieManager cookieManager= CookieManager.getInstance();
            cookieManager.removeAllCookie();
            cookieManager.removeSessionCookie();
            cookieSyncMngr.stopSync();
            cookieSyncMngr.sync();
        }
    }

    public void goToFollowersGainedScreen() {
        Intent followersGainedIntent = new Intent(this, followersGained.class);
        followersGainedIntent.putExtra("VIEWER_ID", viewerId);
        followersGainedIntent.putExtra("VIEWER_FULL_NAME", viewerFullName);
        startActivity(followersGainedIntent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();
    }

    public void goToUsersFollowedScreen() {
        Intent usersFollowedIntent = new Intent(this, usersFollowed.class);
        usersFollowedIntent.putExtra("VIEWER_ID", viewerId);
        usersFollowedIntent.putExtra("VIEWER_FULL_NAME", viewerFullName);
        startActivity(usersFollowedIntent);
        overridePendingTransition(R.anim.enter, R.anim.exit);
        finish();
    }

    public void showPopup(View v) {
        dimLayout.getForeground().setAlpha(70); // dim

        Context wrapper = new ContextThemeWrapper(this, R.style.myPopupMenuStyle);
        PopupMenu popup = new PopupMenu(wrapper, v);

        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.dropdown, popup.getMenu());

        popup.setOnDismissListener(new PopupMenu.OnDismissListener() {
            @Override
            public void onDismiss(PopupMenu menu) {
                dimLayout.getForeground().setAlpha(0);
            }
        });

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.followersGainedScreen) {
                    goToFollowersGainedScreen();
                }

                if (item.getItemId() == R.id.followingScreen) {
                    goToUsersFollowedScreen();
                }
                return true;
            }
        });

        popup.show();
    }
}