package com.instagaon.instagaon;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class usersFollowed extends AppCompatActivity {
    private String viewerId;
    private String viewerFullName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_users_followed);
        viewerId = getIntent().getStringExtra("VIEWER_ID");
        viewerFullName = getIntent().getStringExtra("VIEWER_FULL_NAME");
    }

    @Override
    public void onBackPressed() {
        goBackToGenTckt();
    }

    public void backIconClick(View v) {
        goBackToGenTckt();
    }

    public void goBackToGenTckt () {
        Intent genTicketIntent = new Intent(this, GenTicket.class);
        genTicketIntent.putExtra("VIEWER_ID", viewerId);
        genTicketIntent.putExtra("VIEWER_FULL_NAME", viewerFullName);
        startActivity(genTicketIntent);
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left);
        finish();
    }
}
